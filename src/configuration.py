from argparse import Action, ArgumentParser, RawDescriptionHelpFormatter, ArgumentTypeError
import string
import os
from six.moves.urllib.parse import urlparse


class FallbackToEnvironment(Action):
    def __init__(self, environment_variables, environment, **kwargs):
        default = None

        if environment and environment_variables:
            for var in environment_variables:
                default = environment[var] if var in environment else default

        super(FallbackToEnvironment, self).__init__(default=default, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)


class Configuration:

    @classmethod
    def load(cls, argv, environment):
        description = """
Run GitLab DAST against a target website of your choice.

Arguments can be supplied via command line and will fallback to using the outlined environment variable.
"""

        parser = ArgumentParser(description=description,
                                formatter_class=RawDescriptionHelpFormatter)

        # DAST configuration options
        parser.add_argument('-t', dest='target', type=cls.__url, action=FallbackToEnvironment, environment=environment,
                            environment_variables=['DAST_WEBSITE'],
                            help='The URL of the website to scan (env DAST_WEBSITE)')
        parser.add_argument('--auth-url', dest='auth_url', type=cls.__url, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_URL', 'AUTH_URL'],
                            help='login form URL (env DAST_AUTH_URL)')
        parser.add_argument('--auth-username', dest='auth_username', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME', 'AUTH_USERNAME'],
                            help='login form username (env DAST_USERNAME)')
        parser.add_argument('--auth-password', dest='auth_password', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD', 'AUTH_PASSWORD'],
                            help='login form password (env DAST_PASSWORD)')
        parser.add_argument('--auth-username-field', dest='auth_username_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME_FIELD', 'AUTH_USERNAME_FIELD'],
                            help='login form name of username input field (env DAST_USERNAME_FIELD)')
        parser.add_argument('--auth-password-field', dest='auth_password_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD_FIELD', 'AUTH_PASSWORD_FIELD'],
                            help='login form name of password input field (env DAST_PASSWORD_FIELD)')
        parser.add_argument('--auth-submit-field', dest='auth_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SUBMIT_FIELD', 'AUTH_SUBMIT_FIELD'],
                            help='login form name or value of submit input (env AUTH_SUBMIT_FIELD)')
        parser.add_argument('--auth-first-submit-field', dest='auth_first_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FIRST_SUBMIT_FIELD', 'AUTH_FIRST_SUBMIT_FIELD'],
                            help='login form name or value of submit input of first page (env AUTH_FIRST_SUBMIT_FIELD)')
        parser.add_argument('--auth-display', dest='auth_display', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_DISPLAY', 'AUTH_DISPLAY'],
                            help='set the virtual display to be visible (env AUTH_DISPLAY)')
        parser.add_argument('--auth-auto', dest='auth_auto', type=bool, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_AUTO', 'AUTH_AUTO'],
                            help='login for automatically finds login fields (env AUTH_AUTO)')
        parser.add_argument('--auth-exclude-urls', dest='auth_exclude_urls', type=cls.__list_of_urls,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_EXCLUDE_URLS', 'AUTH_EXCLUDE_URLS'],
                            help='comma separated list of URLs to exclude, no spaces in between, supply all ' +
                                 'URLs causing logout (env AUTH_EXCLUDE_URLS)')
        parser.add_argument('--full-scan',
                            dest='full_scan',
                            type=cls.__truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_ENABLED'],
                            help='Run a ZAP Full Scan: https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan')
        parser.add_argument('--validate-domain',
                            dest='full_scan_domain_validation_required',
                            type=cls.__truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED'],
                            help='Checks the domain has the required headers for running a full-scan:' +
                                 'https://docs.gitlab.com/ee/user/application_security/dast/index.html' +
                                 '#domain-validation')
        parser.add_argument('--availability-timeout',
                            dest='availability_timeout',
                            type=int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_TARGET_AVAILABILITY_TIMEOUT'],
                            help='Time limit in seconds to wait for target availability')

        # ZAP configuration options
        parser.add_argument('-c', dest='zap_config_file',
                            help='zap: config file to use to INFO, IGNORE or FAIL warnings')
        parser.add_argument('-u', dest='zap_config_url',
                            help='zap: URL of config file to use to INFO, IGNORE or FAIL warnings')
        parser.add_argument('-g', dest='zap_gen_file',
                            help='zap: generate default config file (all rules set to WARN)')
        parser.add_argument('-m', dest='zap_mins',
                            help='zap: the number of minutes to spider for (default 1)')
        parser.add_argument('-r', dest='zap_report_html',
                            help='zap: file to write the full ZAP HTML report')
        parser.add_argument('-w', dest='zap_report_md',
                            help='zap: file to write the full ZAP Wiki (Markdown) report')
        parser.add_argument('-x', dest='zap_report_xml',
                            help='zap: file to write the full ZAP XML report')
        parser.add_argument('-a', dest='zap_include_alpha', action='store_true',
                            help='zap: include the alpha passive scan rules as well')
        parser.add_argument('-d', dest='zap_debug', action='store_true',
                            help='zap: show debug messages')
        parser.add_argument('-P', dest='zap_port',
                            help='zap: specify listen port')
        parser.add_argument('-D', dest='zap_delay_in_seconds',
                            help='zap: delay in seconds to wait for passive scanning')
        parser.add_argument('-i', dest='zap_default_info', action='store_true',
                            help='zap: default rules not in the config file to INFO')
        parser.add_argument('-I', dest='zap_no_fail_on_warn', action='store_true',
                            help='zap: do not return failure on warning')
        parser.add_argument('-j', dest='zap_use_ajax_spider', action='store_true',
                            help='zap: use the Ajax spider in addition to the traditional one')
        parser.add_argument('-l', dest='zap_min_level',
                            help='zap: minimum level to show: PASS, IGNORE, INFO, WARN or FAIL, use '
                                 + 'with -s to hide example URLs')
        parser.add_argument('-n', dest='zap_context_file',
                            help='zap: context file which will be loaded prior to spidering the target')
        parser.add_argument('-p', dest='zap_progress_file',
                            help='zap: progress file which specifies issues that are being addressed')
        parser.add_argument('-s', dest='zap_short_format', action='store_true',
                            help='zap: short output format - dont show PASSes or example URLs')
        parser.add_argument('-T', dest='zap_mins_to_wait',
                            help='zap: max time in minutes to wait for ZAP to start and the passive scan to run')
        parser.add_argument('-z', dest='zap_other_options',
                            help='zap: ZAP command line options e.g. -z"-config aaa=bbb -config ccc=ddd"')

        values = parser.parse_args(argv)

        return values

    @classmethod
    def __truthy(cls, value):
        return value == "true" or value == "True" or value == "1"

    @classmethod
    def __url(cls, value):
        try:
            result = urlparse(value)
            if all([result.scheme, result.netloc]) is not True:
                raise ArgumentTypeError("%s is not a valid URL" % value)
            else:
                return value
        except Exception:
            raise ArgumentTypeError("%s is not a valid URL" % value)

    @classmethod
    def __list_of_urls(cls, values):
        urls = [url.strip() for url in values.split(',')]
        return list(map(cls.__url, urls))
