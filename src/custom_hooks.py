import json
import logging
import os
from dast_exception import DastException
import types


class CustomHooks():

    WRK_DIR = '/zap/wrk/'

    def __init__(self, webdriver_factory, zaproxy, dast_report_formatter, system):
        self.webdriver_factory = webdriver_factory
        self.opts = None
        self.json_report = None
        self.zaproxy = zaproxy
        self.dast_report_formatter = dast_report_formatter
        self.system = system

    def zap_access_target(self, zap, target):
        logging.debug('zap_access_target')

        self.zaproxy.new_session(zap, target)

        webdriver = self.webdriver_factory()

        try:
            if webdriver.is_authentication_required():
                webdriver.setup_webdriver(zap, target)
                webdriver.login(zap, target)
        except Exception as e:
            logging.error('Authentication Error')
            logging.exception(e)
        finally:
            webdriver.cleanup()

    def cli_opts(self, opts):
        logging.info('Script params: ' + str(opts))
        self.opts = opts

        json_report = [x[1] for x in self.opts if x[0] == '-J']
        self.json_report = json_report[0] if json_report else None

    def zap_pre_shutdown(self, zap):
        logging.debug('zap_pre_shutdown')

        if not self.json_report:  # no JSON report is generated
            return

        all_scans = zap.spider.scans
        if not all_scans:
            raise DastException('Error: no scans were performed. ' +
                                'Check log file zap.out to find out the reason.')

        # baseline scan and full scan should only perform one scan,
        # warn if for some reason multiple scans were performed
        if len(all_scans) > 1:
            logging.warning('More than one scan was executed. ' +
                            'Reporting only results from first run.')

        scan = all_scans[0]
        spider_scan_results = zap.spider.full_results(int(scan['id']))

        # add scanned URLs to the report file
        with open(os.path.join(self.WRK_DIR, self.json_report), 'r+') as file:
            report = self.dast_report_formatter.format(json.load(file), scan, spider_scan_results)
            file.seek(0)
            json.dump(report, file)
            file.truncate()

    def pre_exit(self, fail_count, warn_count, pass_count):
        # ZAP exits with an exit code based on pass/warn/fail counts.
        # We exit here so that we can exit with a 0 if the scan made it this far (i.e. ran successfully)
        self.system.exit(0)
