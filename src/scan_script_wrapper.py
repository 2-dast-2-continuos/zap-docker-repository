import os
import argparse
from itertools import chain
from dast_exception import DastException


class ScanScriptWrapper():

    def __init__(self, original_main, config, log, target_website, system):
        self.original_main = original_main
        self.config = config
        self.log = log
        self.target_website = target_website
        self.system = system

    def run(self):
        if not self.target_website.is_configured():
            self.log.error('Target website is not configured.')
            self.log.error('See https://docs.gitlab.com/ee/user/application_security/dast/ for instructions '
                           'on how to configure DAST, or run `/analyze --help`.')
            return self.system.exit(1)

        self.log.info('using Python {}'.format(self.system.version()))
        self.log.info('waiting for {} to be available'.format(self.target_website.address()))

        site_check = self.target_website.check_site_is_available()

        if not site_check.is_available():
            self.log.warn("{} could not be reached, attempting scan anyway".format(self.config.target))
            self.log.debug("last attempted access of target caused error {}".format(site_check.unavailable_reason()))

        is_safe_to_scan, safety_error = site_check.is_safe_to_scan()

        if not is_safe_to_scan:
            self.log.error('domain validation failed due to: {}, see {}'.format(
                safety_error,
                'https://docs.gitlab.com/ee/user/application_security/dast/#domain-validation'))
            return self.system.exit(1)

        self.log.info("starting scan")

        # Hide "Starting new HTTP connection" messages
        self.log.getLogger("requests").setLevel(self.log.DEBUG)

        try:
            self.original_main.main(self.zap_arguments())
        except Exception:
            raise
        else:
            pass
        finally:
            pass

    def zap_arguments(self):
        args = [
            self.__get_argument('target', '-t'),
            ['-J', 'gl-dast-report.json'],
            self.__get_argument('zap_config_file', '-c'),
            self.__get_argument('zap_config_url', '-u'),
            self.__get_argument('zap_gen_file', '-g'),
            self.__get_argument('zap_mins', '-m'),
            self.__get_argument('zap_report_html', '-r'),
            self.__get_argument('zap_report_md', '-w'),
            self.__get_argument('zap_report_xml', '-x'),
            self.__get_argument('zap_include_alpha', '-a', boolean_flag=True),
            self.__get_argument('zap_debug', '-d', boolean_flag=True),
            self.__get_argument('zap_port', '-P'),
            self.__get_argument('zap_delay_in_seconds', '-D'),
            self.__get_argument('zap_default_info', '-i', boolean_flag=True),
            self.__get_argument('zap_no_fail_on_warn', '-I', boolean_flag=True),
            self.__get_argument('zap_use_ajax_spider', '-j', boolean_flag=True),
            self.__get_argument('zap_min_level', '-l'),
            self.__get_argument('zap_context_file', '-n'),
            self.__get_argument('zap_progress_file', '-p'),
            self.__get_argument('zap_short_format', '-s', boolean_flag=True),
            self.__get_argument('zap_mins_to_wait', '-T'),
            self.__get_argument('zap_other_options', '-z')]

        return list(chain.from_iterable(args))

    def __get_argument(self, name, option_name, boolean_flag=False):
        option = getattr(self.config, name, None)

        if option:
            if boolean_flag:
                return [option_name]
            return [option_name, getattr(self.config, name)]
        return []
