import sys


class System():

    def notify(self, message):
        print(message)

    def version(self):
        return " ".join([x.strip() for x in sys.version.splitlines()])

    def exit(self, exit_code):
        sys.exit(exit_code)
