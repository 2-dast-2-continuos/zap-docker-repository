{
  "@generated": "Tue, 22 Oct 2019 01:50:28",
  "@version":   "D-2019-09-23",
  "site":       [
    {
      "@host":  "nginx",
      "@name":  "http://nginx",
      "@port":  "80",
      "@ssl":   "false",
      "alerts": [
        {
          "alert":      "Anti CSRF Tokens Scanner",
          "confidence": "2",
          "count":      "1",
          "cweid":      "352",
          "desc":       "<p>A cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf.</p><p></p><p>CSRF attacks are effective in a number of situations, including:</p><p>    * The victim has an active session on the target site.</p><p>    * The victim is authenticated via HTTP auth on the target site.</p><p>    * The victim is on the same local network as the target site.</p><p></p><p>CSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.</p>",
          "instances":  [
            {
              "attack": "",
              "evidence": "<form action=\"/myform\" method=\"POST\">",
              "method":   "GET",
              "param":    "",
              "uri":      "http://nginx"
            }
          ],
          "name":       "Anti CSRF Tokens Scanner",
          "otherinfo":  "",
          "pluginid":   "20012",
          "reference":  "<p>http://projects.webappsec.org/Cross-Site-Request-Forgery</p><p>http://cwe.mitre.org/data/definitions/352.html</p>",
          "riskcode":   "3",
          "riskdesc":   "High (Medium)",
          "solution":   "<p>Phase: Architecture and Design</p><p>Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid.</p><p>For example, use anti-CSRF packages such as the OWASP CSRFGuard.</p><p></p><p>Phase: Implementation</p><p>Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script.</p><p></p><p>Phase: Architecture and Design</p><p>Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330).</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Identify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation.</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Use the ESAPI Session Management control.</p><p>This control includes a component for CSRF.</p><p></p><p>Do not use the GET method for any request that triggers a state change.</p><p></p><p>Phase: Implementation</p><p>Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons.</p>",
          "sourceid":   "1",
          "wascid":     "9"
        },
        {
          "alert":      "X-Frame-Options Header Not Set",
          "confidence": "2",
          "count":      "2",
          "cweid":      "16",
          "desc":       "<p>X-Frame-Options header is not included in the HTTP response to protect against 'ClickJacking' attacks.</p>",
          "instances":  [
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-Frame-Options",
              "uri":      "http://nginx/"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-Frame-Options",
              "uri":      "http://nginx"
            }
          ],
          "name":       "X-Frame-Options Header Not Set",
          "otherinfo":  "",
          "pluginid":   "10020",
          "reference":  "<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/combating-clickjacking-with-x-frame-options.aspx</p>",
          "riskcode":   "2",
          "riskdesc":   "Medium (Medium)",
          "solution":   "<p>Most modern Web browsers support the X-Frame-Options HTTP header. Ensure it's set on all web pages returned by your site (if you expect the page to be framed only by pages on your server (e.g. it's part of a FRAMESET) then you'll want to use SAMEORIGIN, otherwise if you never expect the page to be framed, you should use DENY. ALLOW-FROM allows specific websites to frame the web page in supported web browsers).</p>",
          "sourceid":   "3",
          "wascid":     "15"
        },
        {
          "alert":      "Absence of Anti-CSRF Tokens",
          "confidence": "2",
          "count":      "2",
          "cweid":      "352",
          "desc":       "<p>No Anti-CSRF tokens were found in a HTML submission form.</p><p>A cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf.</p><p></p><p>CSRF attacks are effective in a number of situations, including:</p><p>    * The victim has an active session on the target site.</p><p>    * The victim is authenticated via HTTP auth on the target site.</p><p>    * The victim is on the same local network as the target site.</p><p></p><p>CSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.</p>",
          "instances":  [
            {
              "attack": "",
              "evidence": "<form action=\"/myform\" method=\"POST\">",
              "method":   "GET",
              "param":    "",
              "uri":      "http://nginx/"
            },
            {
              "attack": "",
              "evidence": "<form action=\"/myform\" method=\"POST\">",
              "method":   "GET",
              "param":    "",
              "uri":      "http://nginx"
            }
          ],
          "name":       "Absence of Anti-CSRF Tokens",
          "otherinfo":  "<p>No known Anti-CSRF token [anticsrf, CSRFToken, __RequestVerificationToken, csrfmiddlewaretoken, authenticity_token, OWASP_CSRFTOKEN, anoncsrf, csrf_token, _csrf, _csrfSecret] was found in the following HTML form: [Form 1: \"name\" ].</p>",
          "pluginid":   "10202",
          "reference":  "<p>http://projects.webappsec.org/Cross-Site-Request-Forgery</p><p>http://cwe.mitre.org/data/definitions/352.html</p>",
          "riskcode":   "1",
          "riskdesc":   "Low (Medium)",
          "solution":   "<p>Phase: Architecture and Design</p><p>Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid.</p><p>For example, use anti-CSRF packages such as the OWASP CSRFGuard.</p><p></p><p>Phase: Implementation</p><p>Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script.</p><p></p><p>Phase: Architecture and Design</p><p>Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330).</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Identify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation.</p><p>Note that this can be bypassed using XSS.</p><p></p><p>Use the ESAPI Session Management control.</p><p>This control includes a component for CSRF.</p><p></p><p>Do not use the GET method for any request that triggers a state change.</p><p></p><p>Phase: Implementation</p><p>Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons.</p>",
          "sourceid":   "3",
          "wascid":     "9"
        },
        {
          "alert":      "Web Browser XSS Protection Not Enabled",
          "confidence": "2",
          "count":      "5",
          "cweid":      "933",
          "desc":       "<p>Web Browser XSS Protection is not enabled, or is disabled by the configuration of the 'X-XSS-Protection' HTTP response header on the web server</p>",
          "instances":  [
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-XSS-Protection",
              "uri":      "http://nginx/"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "POST",
              "param":    "X-XSS-Protection",
              "uri":      "http://nginx/myform"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-XSS-Protection",
              "uri":      "http://nginx/robots.txt"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-XSS-Protection",
              "uri":      "http://nginx/sitemap.xml"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-XSS-Protection",
              "uri":      "http://nginx"
            }
          ],
          "name":       "Web Browser XSS Protection Not Enabled",
          "otherinfo":  "<p>The X-XSS-Protection HTTP response header allows the web server to enable or disable the web browser's XSS protection mechanism. The following values would attempt to enable it: </p><p>X-XSS-Protection: 1; mode=block</p><p>X-XSS-Protection: 1; report=http://www.example.com/xss</p><p>The following values would disable it:</p><p>X-XSS-Protection: 0</p><p>The X-XSS-Protection HTTP response header is currently supported on Internet Explorer, Chrome and Safari (WebKit).</p><p>Note that this alert is only raised if the response body could potentially contain an XSS payload (with a text-based content type, with a non-zero length).</p>",
          "pluginid":   "10016",
          "reference":  "<p>https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet</p><p>https://www.veracode.com/blog/2014/03/guidelines-for-setting-security-headers/</p>",
          "riskcode":   "1",
          "riskdesc":   "Low (Medium)",
          "solution":   "<p>Ensure that the web browser's XSS filter is enabled, by setting the X-XSS-Protection HTTP response header to '1'.</p>",
          "sourceid":   "3",
          "wascid":     "14"
        },
        {
          "alert":      "X-Content-Type-Options Header Missing",
          "confidence": "2",
          "count":      "2",
          "cweid":      "16",
          "desc":       "<p>The Anti-MIME-Sniffing header X-Content-Type-Options was not set to 'nosniff'. This allows older versions of Internet Explorer and Chrome to perform MIME-sniffing on the response body, potentially causing the response body to be interpreted and displayed as a content type other than the declared content type. Current (early 2014) and legacy versions of Firefox will use the declared content type (if one is set), rather than performing MIME-sniffing.</p>",
          "instances":  [
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-Content-Type-Options",
              "uri":      "http://nginx/"
            },
            {
              "attack": "",
              "evidence": "",
              "method":   "GET",
              "param":    "X-Content-Type-Options",
              "uri":      "http://nginx"
            }
          ],
          "name":       "X-Content-Type-Options Header Missing",
          "otherinfo":  "<p>This issue still applies to error type pages (401, 403, 500, etc) as those pages are often still affected by injection issues, in which case there is still concern for browsers sniffing pages away from their actual content type.</p><p>At \"High\" threshold this scanner will not alert on client or server error responses.</p>",
          "pluginid":   "10021",
          "reference":  "<p>http://msdn.microsoft.com/en-us/library/ie/gg622941%28v=vs.85%29.aspx</p><p>https://www.owasp.org/index.php/List_of_useful_HTTP_headers</p>",
          "riskcode":   "1",
          "riskdesc":   "Low (Medium)",
          "solution":   "<p>Ensure that the application/web server sets the Content-Type header appropriately, and that it sets the X-Content-Type-Options header to 'nosniff' for all web pages.</p><p>If possible, ensure that the end user uses a standards-compliant and modern web browser that does not perform MIME-sniffing at all, or that can be directed by the web application/web server to not perform MIME-sniffing.</p>",
          "sourceid":   "3",
          "wascid":     "15"
        }
      ]
    }
  ],
  "spider":     {
    "progress": "100",
    "result":   {
      "urlsInScope":    [
        {
          "method":             "GET",
          "processed":          "true",
          "reasonNotProcessed": "",
          "statusCode":         "200",
          "statusReason":       "OK",
          "url":                "http://nginx/"
        },
        {
          "method":             "POST",
          "processed":          "true",
          "reasonNotProcessed": "",
          "statusCode":         "404",
          "statusReason":       "Not Found",
          "url":                "http://nginx/myform"
        },
        {
          "method":             "GET",
          "processed":          "true",
          "reasonNotProcessed": "",
          "statusCode":         "404",
          "statusReason":       "Not Found",
          "url":                "http://nginx/robots.txt"
        },
        {
          "method":             "GET",
          "processed":          "true",
          "reasonNotProcessed": "",
          "statusCode":         "404",
          "statusReason":       "Not Found",
          "url":                "http://nginx/sitemap.xml"
        },
        {
          "method":             "GET",
          "processed":          "true",
          "reasonNotProcessed": "",
          "statusCode":         "200",
          "statusReason":       "OK",
          "url":                "http://nginx"
        }
      ],
      "urlsIoError":    [],
      "urlsOutOfScope": []
    },
    "state":    "FINISHED"
  }
}
