# GitLab DAST changelog

## v1.5.4
- Validate URL command line arguments (!69)
- Exit code is zero when a scan runs successfully, non zero when a scan fails or arguments are invalid (!69)
- The DAST report is deterministic, keys in the JSON document are in alphabetical order (!70)
- Ajax scans start from the target URL (!71)
- Don't verify HTTPS certificates when determining if a target site is ready for scanning (!76) 

## v1.5.3
- Fixed support for `--auth-exclude-urls` parameter (!52)

## v1.5.2
- DAST depends on ZAP weekly release w2019-09-24, re-enabling `ascanrulesBeta` rules
- Removed Python 3 to fix Full Scans

## v1.5.1
- DAST depends on ZAP release 2.8.0 (!50)

## v1.5.0
- Running `/analyze --help` shows all options and environment variables supported by DAST (!39)
- Expose ZAP logs while executing scans (!42)

## v1.4.0
- Implement [domain validation](https://docs.gitlab.com/ee/user/application_security/dast/index.html#domain-validation) option for full scans (!35)

## v1.3.0
- Report which URLs were scanned (!24)

## v1.2.7
- Fix passing of optional params to ZAP (!27)

## v1.2.6
- Fix max. curl timeout to be longer than 150 seconds (!26)

## v1.2.5
- Fix curl timeout (!25)

## v1.2.4
- Fix timeout when $DAST_TARGET_AVAILABILITY_TIMEOUT is used (!21)

## v1.2.3
- Fix auto login functionality. Auto login is used if the HTML elements for username, password, or submit button have not been specified.

## v1.2.2
- Fix a bug where `analyze` would fail if only `DAST_WEBSITE` was used. https://gitlab.com/gitlab-org/gitlab-ee/issues/11744

## v1.2.1
- Accept $DAST_WEBSITE env var instead of `-t` parameter (still supported for backward compatibility)

## v1.2.0
- Add [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) support (!14)

## v1.1.2
- Add workaround for supporting long CLI auth options (!9)

## v1.1.1
- Fix a problem with multiple login buttons on the login page.

## v1.1.0
- First release of the DAST GitLab image.
